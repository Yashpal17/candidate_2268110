package com.gov.support;

import com.gov.browsers.WebBrowserFactory;
import com.gov.httpVerbs.BaseVerb;
import com.gov.httpVerbs.GetVerb;
import com.gov.pages.BasePage;
import com.gov.pages.UKVisaConfirmation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class WorldHelper {
    private WebDriver driver = WebBrowserFactory.getThreadedDriver();
    private static BasePage basePage = null;
    private static UKVisaConfirmation uKVisaConfirmation = null;
    private static BaseVerb baseVerb = null;
    private static GetVerb getVerb = null;

    public BasePage getBasePage(){
        if(basePage != null) return basePage;
        return PageFactory.initElements(driver, BasePage.class);
    }

    public UKVisaConfirmation getUKVisaConfirmation(){
        if(uKVisaConfirmation != null) return uKVisaConfirmation;
        return PageFactory.initElements(driver, UKVisaConfirmation.class);
    }

    public BaseVerb getBaseVerb(){
        if(baseVerb != null) return baseVerb;
        return PageFactory.initElements(driver, BaseVerb.class);
    }

    public GetVerb getGetVerb(){
        if(getVerb != null) return getVerb;
        return PageFactory.initElements(driver, GetVerb.class);
    }

}
