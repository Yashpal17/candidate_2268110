package com.gov.httpVerbs;

import io.restassured.response.Response;

import static java.lang.String.format;

public class GetVerb extends BaseVerb{

    public Response getResponse() {
        return response;
    }

    public Response getApi(String uri) {
        response = setUp().get(uri);

        printResponse(format("\nGET %s, \nresponse: %s\n", uri, response.asString()));

        return response;
    }
}
