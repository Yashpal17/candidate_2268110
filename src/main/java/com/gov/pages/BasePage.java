package com.gov.pages;

import com.gov.utilities.EnvConfig;
import com.gov.webControl.ButtonControl;
import cucumber.api.Scenario;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class BasePage {

    protected WebDriver webDriver;
    @FindBy(css = "#global-cookie-message > div.gem-c-cookie-banner__wrapper.govuk-width-container > div > div > div.gem-c-cookie-banner__buttons > div.gem-c-cookie-banner__button.gem-c-cookie-banner__button-accept.govuk-grid-column-full.govuk-grid-column-one-half-from-desktop > button")
    private WebElement acceptCookies = null;
    @FindBy(linkText = "Start now")
    private WebElement clickStartButton = null;

    public BasePage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    /*public BasePage deleteAllCookies() {
        manage().window().maximize();
        manage().deleteAllCookies();
        String baseUrl = EnvConfig.getValue("base.url");
        webDriver.navigate().to(baseUrl);
        return this;
    }*/

    public UKVisaConfirmation acceptAllCookies() {
        ButtonControl.clickButton(acceptCookies);
        return PageFactory.initElements(webDriver,UKVisaConfirmation.class);
    }

    public UKVisaConfirmation clickStartNowButton() {
        ButtonControl.clickButton(clickStartButton);
        return PageFactory.initElements(webDriver,UKVisaConfirmation.class);
    }
}
