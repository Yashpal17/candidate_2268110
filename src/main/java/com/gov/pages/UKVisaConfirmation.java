package com.gov.pages;

import com.gov.webControl.ButtonControl;
import com.gov.webControl.DropdownControl;
import com.gov.webControl.RadioControl;
import com.gov.webWaits.Waits;
import javafx.scene.input.InputMethodTextRun;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UKVisaConfirmation extends BasePage{

    @FindBy(id="response")
    private WebElement enterCountry = null;
    @FindBy(css = "#current-question > button")
    private WebElement nextStepButton= null;
    @FindBy(css = "#current-question > div > fieldset > legend > h1")
    private WebElement question1 = null;
    @FindBy(id="response-1")
    private WebElement moreThan6Months = null;
    @FindBy(xpath = "/html/body[1]/div[6]/main[1]/div[1]/div[1]/div[2]")
    private WebElement visaReqForStudy = null;
    @FindBy(css = "#current-question > div > fieldset > div.govuk-radios")
    private WebElement visitReason = null;


    public UKVisaConfirmation(WebDriver webDriver){
        super(webDriver);
    }

    public UKVisaConfirmation selectCountry(String country) {
        DropdownControl.selectDropDownByVisibleText(enterCountry,country);
        Waits.waitForNoOfSeconds(1);
        ButtonControl.clickButton(nextStepButton);
        return this;
    }

    public UKVisaConfirmation enterPurposeAndMoreDetails(String visit) {
        Waits.waitForNoOfSeconds(2);
try {
    for (WebElement label : visitReason.findElements(By.tagName("div"))) {
        for (WebElement reason : label.findElements(By.tagName("label"))) {
            if (reason.getText().contains(visit)) {
                reason.click();
                ButtonControl.clickButton(nextStepButton);
                Waits.waitForNoOfSeconds(2);
                if (question1.getText().contains("How long are you planning to study in the UK for?")) {
                    RadioControl.clickButton(moreThan6Months);
                } else if (question1.getText().contains("Will you be travelling with or visiting either your partner or a family member in the UK?")) {
                    Waits.waitForNoOfSeconds(1);
                    RadioControl.clickButton(moreThan6Months);
                }
                Waits.waitForNoOfSeconds(2);
            }

        }
    }

}catch(org.openqa.selenium.StaleElementReferenceException ex){
}
        return this;
    }

    public void submitForm() {
        ButtonControl.clickButton(nextStepButton);
    }

    public boolean checkUKVisaStatusForStudy() {
        Waits.waitForNoOfSeconds(3);
        if(visaReqForStudy.getText().contains("You'll need a visa to study in the UK")){
            System.out.println("You'll need a visa to study in the UK");
            return true;
        }else if(visaReqForStudy.getText().contains("You will not need a visa to come to the UK")) {
            System.out.println("You will not need a visa to come to the UK");
            return true;
        }else if(visaReqForStudy.getText().contains("You'll need a visa to come to the UK")){
            System.out.println("You'll need a visa to come to the UK");
            return true;
        }
        return false;
    }
}
