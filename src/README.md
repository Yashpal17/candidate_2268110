**Make sure you havd jdk installed, please use jdk 1.8 and above. If having problem with 15th version, please switch to jdk1.8.192
Open terminal and write java -version to make sure you have java installed.

**To run feature given for web application individually with different browser**
_Chrome_ - To run Check UK Visa Confirmation feature, in TestRunner change the tag @checkUKVisa and right click and choose Run TestRunner by default it will run in Chrome Browser
_FireFox_ - To run Check UK Visa Confirmation feature in FireFox Browser, in environment file src/main/java/resources/environment.properties, change browser = “firefox” and right click and choose Run TestRunner
_Internet Explorer_ - To run Check UK Visa Confirmation feature in Internet Explorer Browser, in environment file src/main/java/resources/environment.properties, change browser = “ie” and right click and choose Run TestRunner
_Edge_ - To run Check UK Visa Confirmation feature in Edge Browser, in environment file src/main/java/resources/environment.properties, change browser = “edge” and right click and choose Run TestRunner

**To run feature given for API -**
Please follow below two steps
*In TestRunner class /src/test/java/com/gov/TestRunner.java, change the tag to @checkPostCode and right click and choose Run TestRunner
*In ScenarioHooks class /src/test/java/com/gov/hooks/ScenarioHooks.java, please comment Before scenario

**To run both the feature together -**
*Please uncomment the Before Scenario if you ran API test cases
*In TestRunner class /src/test/java/com/gov/TestRunner.java, change the tag to @all and right click and choose Run TestRunner

**To review Cucumber Reports -**
*Go to /target/test-report/report-html/index.html and choose browser to open for ex:Chrome
