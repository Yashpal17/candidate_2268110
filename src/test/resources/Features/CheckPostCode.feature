@checkPostCode @all
Feature: Query a postcode and receive a 200 response
  As a backend user
  I want to query a PostCode within the UK
  So I can verify its 200 status coce response

  Scenario: Query a postcode
    When I enter the Postcode
    Then I should get the 200 status code response
