package com.gov.steps;

import com.gov.pages.BasePage;
import com.gov.pages.UKVisaConfirmation;
import com.gov.support.WorldHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class CheckUKVisaSteps {

    private WorldHelper helper;
    private BasePage basePage;
    private UKVisaConfirmation ukVisaConfirmation;

    public CheckUKVisaSteps(WorldHelper helper){
        this.helper = helper;
    }

    @Given("^I am on government website to check for UK visa$")
    public void iAmOnGovernmentWebsiteToCheckForUKVisa() throws Throwable {
      basePage = helper.getBasePage();
    }

    @Given("^I accept all the cookies$")
    public void iAcceptAllTheCookies() throws Throwable {
      ukVisaConfirmation = basePage.acceptAllCookies();
    }
    @Given("^I click on 'Start Now' button to check UK Visa required for given country$")
    public void iClickOnStartNowButtonToCheckUKVisaRequiredForGivenCountry() throws Throwable {
        ukVisaConfirmation = basePage.clickStartNowButton();
    }
    @Given("^I provide a nationality of \"([^\"]*)\"$")
    public void iProvideANationalityOf(String country) throws Throwable {
      helper.getUKVisaConfirmation().selectCountry(country);
    }

    @When("^I submit the form$")
    public void iSubmitTheForm() throws Throwable {
      ukVisaConfirmation.submitForm();
    }

    @Given("^I select the reason of \"([^\"]*)\"$")
    public void iSelectTheReasonOf(String visit) throws Throwable {
        helper.getUKVisaConfirmation().enterPurposeAndMoreDetails(visit);
    }

    @Then("^I will be informed about status")
    public void iWillBeInformed() throws Throwable {
        Assert.assertFalse(ukVisaConfirmation.checkUKVisaStatusForStudy());
    }


}
