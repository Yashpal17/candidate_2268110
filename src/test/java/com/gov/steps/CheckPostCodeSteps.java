package com.gov.steps;

import com.gov.httpVerbs.BaseVerb;
import com.gov.support.WorldHelper;
import com.gov.utilities.EnvConfig;
import com.gov.utilities.TestData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;

public class CheckPostCodeSteps {

    private WorldHelper helper;
    private Response response;

    public CheckPostCodeSteps(WorldHelper helper){
        this.helper = helper;
    }

    @When("^I enter the Postcode$")
    public void iEnterThePostcode() throws Throwable {
        String apiEndpoint = EnvConfig.getValue("baseAPI.url");
        String postCode = TestData.getValue("post code");
        String uri = apiEndpoint + postCode;
        response = helper.getGetVerb().getApi(uri);

    }

    @Then("^I should get the (\\d+) status code response$")
    public void iShouldGetTheStatusCodeResponse(int arg1) throws Throwable {
        Assert.assertEquals(response.getStatusCode(),200);
    }
}
