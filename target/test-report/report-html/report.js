$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Features/CheckUKVisa.feature");
formatter.feature({
  "line": 2,
  "name": "Confirm whether a visa is required to visit the UK",
  "description": "As a applicant\nI want to confirm whether a visa is required to visit the UK\nSo I can apply for visa based on confirmation",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@checkUKVisa"
    },
    {
      "line": 1,
      "name": "@all"
    }
  ]
});
formatter.scenarioOutline({
  "line": 12,
  "name": "Check whether a visa is required for various countries",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 13,
  "name": "I provide a nationality of \"\u003cCountry\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I select the reason of \"\u003cVisit\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I submit the form",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I will be informed about status",
  "keyword": "Then "
});
formatter.examples({
  "line": 18,
  "name": "",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries;",
  "rows": [
    {
      "cells": [
        "Country",
        "Visit"
      ],
      "line": 19,
      "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries;;1"
    },
    {
      "cells": [
        "Japan",
        "Study"
      ],
      "line": 20,
      "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries;;2"
    },
    {
      "cells": [
        "Japan",
        "Tourism"
      ],
      "line": 21,
      "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries;;3"
    },
    {
      "cells": [
        "Russia",
        "Tourism"
      ],
      "line": 22,
      "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 487005,
  "status": "passed"
});
formatter.before({
  "duration": 2377557568,
  "status": "passed"
});
formatter.background({
  "line": 7,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 8,
  "name": "I am on government website to check for UK visa",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I accept all the cookies",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I click on \u0027Start Now\u0027 button to check UK Visa required for given country",
  "keyword": "And "
});
formatter.match({
  "location": "CheckUKVisaSteps.iAmOnGovernmentWebsiteToCheckForUKVisa()"
});
formatter.result({
  "duration": 121733389,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iAcceptAllTheCookies()"
});
formatter.result({
  "duration": 136658277,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iClickOnStartNowButtonToCheckUKVisaRequiredForGivenCountry()"
});
formatter.result({
  "duration": 282847081,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Check whether a visa is required for various countries",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@checkUKVisa"
    },
    {
      "line": 1,
      "name": "@all"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "I provide a nationality of \"Japan\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I select the reason of \"Study\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I submit the form",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I will be informed about status",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Japan",
      "offset": 28
    }
  ],
  "location": "CheckUKVisaSteps.iProvideANationalityOf(String)"
});
formatter.result({
  "duration": 1278262782,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Study",
      "offset": 24
    }
  ],
  "location": "CheckUKVisaSteps.iSelectTheReasonOf(String)"
});
formatter.result({
  "duration": 5885574336,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iSubmitTheForm()"
});
formatter.result({
  "duration": 261320580,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iWillBeInformed()"
});
formatter.result({
  "duration": 9063812940,
  "status": "passed"
});
formatter.after({
  "duration": 78800,
  "status": "passed"
});
formatter.before({
  "duration": 47552,
  "status": "passed"
});
formatter.before({
  "duration": 224700736,
  "status": "passed"
});
formatter.background({
  "line": 7,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 8,
  "name": "I am on government website to check for UK visa",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I accept all the cookies",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I click on \u0027Start Now\u0027 button to check UK Visa required for given country",
  "keyword": "And "
});
formatter.match({
  "location": "CheckUKVisaSteps.iAmOnGovernmentWebsiteToCheckForUKVisa()"
});
formatter.result({
  "duration": 305121,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iAcceptAllTheCookies()"
});
formatter.result({
  "duration": 107854666,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iClickOnStartNowButtonToCheckUKVisaRequiredForGivenCountry()"
});
formatter.result({
  "duration": 222806747,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Check whether a visa is required for various countries",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@checkUKVisa"
    },
    {
      "line": 1,
      "name": "@all"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "I provide a nationality of \"Japan\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I select the reason of \"Tourism\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I submit the form",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I will be informed about status",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Japan",
      "offset": 28
    }
  ],
  "location": "CheckUKVisaSteps.iProvideANationalityOf(String)"
});
formatter.result({
  "duration": 1216314398,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Tourism",
      "offset": 24
    }
  ],
  "location": "CheckUKVisaSteps.iSelectTheReasonOf(String)"
});
formatter.result({
  "duration": 3944158613,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"css selector\",\"selector\":\"#current-question \u003e div \u003e fieldset \u003e legend \u003e h1\"}\n  (Session info: chrome\u003d87.0.4280.88)\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027YASHPALs-MBP.broadband\u0027, ip: \u0027fe80:0:0:0:14f3:95c8:eca4:449%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.15.6\u0027, java.version: \u00271.8.0_92\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 87.0.4280.88, chrome: {chromedriverVersion: 87.0.4280.88 (89e2380a3e36c..., userDataDir: /var/folders/hb/h8t44fzn76z...}, goog:chromeOptions: {debuggerAddress: localhost:50180}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 59e84de7456e9890a1b53abdfdb51f47\n*** Element info: {Using\u003dcss selector, value\u003d#current-question \u003e div \u003e fieldset \u003e legend \u003e h1}\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByCssSelector(RemoteWebDriver.java:420)\n\tat org.openqa.selenium.By$ByCssSelector.findElement(By.java:431)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.getText(Unknown Source)\n\tat com.gov.pages.UKVisaConfirmation.enterPurposeAndMoreDetails(UKVisaConfirmation.java:49)\n\tat com.gov.steps.CheckUKVisaSteps.iSelectTheReasonOf(CheckUKVisaSteps.java:46)\n\tat ✽.And I select the reason of \"Tourism\"(Features/CheckUKVisa.feature:14)\n",
  "status": "failed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iSubmitTheForm()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "CheckUKVisaSteps.iWillBeInformed()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 588075550,
  "status": "passed"
});
formatter.before({
  "duration": 37802,
  "status": "passed"
});
formatter.before({
  "duration": 213950233,
  "status": "passed"
});
formatter.background({
  "line": 7,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 8,
  "name": "I am on government website to check for UK visa",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I accept all the cookies",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I click on \u0027Start Now\u0027 button to check UK Visa required for given country",
  "keyword": "And "
});
formatter.match({
  "location": "CheckUKVisaSteps.iAmOnGovernmentWebsiteToCheckForUKVisa()"
});
formatter.result({
  "duration": 267814,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iAcceptAllTheCookies()"
});
formatter.result({
  "duration": 115208799,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iClickOnStartNowButtonToCheckUKVisaRequiredForGivenCountry()"
});
formatter.result({
  "duration": 206411581,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Check whether a visa is required for various countries",
  "description": "",
  "id": "confirm-whether-a-visa-is-required-to-visit-the-uk;check-whether-a-visa-is-required-for-various-countries;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@checkUKVisa"
    },
    {
      "line": 1,
      "name": "@all"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "I provide a nationality of \"Russia\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I select the reason of \"Tourism\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I submit the form",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I will be informed about status",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Russia",
      "offset": 28
    }
  ],
  "location": "CheckUKVisaSteps.iProvideANationalityOf(String)"
});
formatter.result({
  "duration": 1260852371,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Tourism",
      "offset": 24
    }
  ],
  "location": "CheckUKVisaSteps.iSelectTheReasonOf(String)"
});
formatter.result({
  "duration": 6747037505,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iSubmitTheForm()"
});
formatter.result({
  "duration": 76491301,
  "status": "passed"
});
formatter.match({
  "location": "CheckUKVisaSteps.iWillBeInformed()"
});
formatter.result({
  "duration": 9020025032,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"/html/body[1]/div[6]/main[1]/div[1]/div[1]/div[2]\"}\n  (Session info: chrome\u003d87.0.4280.88)\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027YASHPALs-MBP.broadband\u0027, ip: \u0027fe80:0:0:0:14f3:95c8:eca4:449%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.15.6\u0027, java.version: \u00271.8.0_92\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 87.0.4280.88, chrome: {chromedriverVersion: 87.0.4280.88 (89e2380a3e36c..., userDataDir: /var/folders/hb/h8t44fzn76z...}, goog:chromeOptions: {debuggerAddress: localhost:50180}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 59e84de7456e9890a1b53abdfdb51f47\n*** Element info: {Using\u003dxpath, value\u003d/html/body[1]/div[6]/main[1]/div[1]/div[1]/div[2]}\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:428)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy21.getText(Unknown Source)\n\tat com.gov.pages.UKVisaConfirmation.checkUKVisaStatusForStudy(UKVisaConfirmation.java:72)\n\tat com.gov.steps.CheckUKVisaSteps.iWillBeInformed(CheckUKVisaSteps.java:51)\n\tat ✽.Then I will be informed about status(Features/CheckUKVisa.feature:16)\n",
  "status": "failed"
});
formatter.embedding("image/png", "embedded1.png");
formatter.after({
  "duration": 470915026,
  "status": "passed"
});
});