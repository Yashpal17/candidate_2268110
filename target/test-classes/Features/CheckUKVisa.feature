@checkUKVisa @all
Feature:  Confirm whether a visa is required to visit the UK
  As a applicant
  I want to confirm whether a visa is required to visit the UK
  So I can apply for visa based on confirmation

  Background:
    Given I am on government website to check for UK visa
    And I accept all the cookies
    And I click on 'Start Now' button to check UK Visa required for given country

  Scenario Outline: Check whether a visa is required for various countries
    Given I provide a nationality of "<Country>"
    And I select the reason of "<Visit>"
    When I submit the form
    Then I will be informed about status

    Examples:
      |  Country |  Visit    |
      |  Japan   |  Study    |
      | Japan    | Tourism   |
      | Russia   | Tourism   |